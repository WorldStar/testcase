/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 10.1.13-MariaDB : Database - testcase_1
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`testcase_1` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `testcase_1`;

/*Table structure for table `star_cities` */

DROP TABLE IF EXISTS `star_cities`;

CREATE TABLE `star_cities` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `star_cities` */

insert  into `star_cities`(`id`,`name`) values (1,'City 1'),(2,'City 2'),(3,'City 3'),(4,'City 4');

/*Table structure for table `star_users` */

DROP TABLE IF EXISTS `star_users`;

CREATE TABLE `star_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `street` text NOT NULL,
  `zipcode` int(11) NOT NULL,
  `city_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

/*Data for the table `star_users` */

insert  into `star_users`(`id`,`name`,`firstname`,`street`,`zipcode`,`city_id`) values (32,'test 1','world star','Liaoning, china',1118000,2),(33,'test 2','world star 1','city 1, Liaoning, china',1118000,3),(34,'test 3','world star 2','city 2, Liaoning, china',1118000,3),(35,'test 4','world star 3','city 3, Liaoning, china',1118000,4),(36,'test 5','world star 4','city 4, Liaoning, china',1118000,1),(37,'test 6','world star','Liaoning, china',1118000,3),(38,'test 7','world star','Liaoning, china',1118000,2);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
